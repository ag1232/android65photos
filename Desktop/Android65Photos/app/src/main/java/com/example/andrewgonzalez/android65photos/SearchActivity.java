package com.example.andrewgonzalez.android65photos;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.view.View;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;

public class SearchActivity extends AppCompatActivity {

    final Context context = this;
    ArrayList<Album> myAlbums;
    ArrayList<Photo> found;
    Album currAlbum;
    ListAdapter PhotoAdapter;
    ListView PhotoListView;
    File afile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        found = new ArrayList<Photo>();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        Toolbar myToolbar = (Toolbar)findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);

        afile = new File(this.context.getFilesDir() + File.separator + "albums.dat");
        System.out.println(afile.exists());

        try {
            FileInputStream fileIn = new FileInputStream(afile);
            ObjectInputStream in = new ObjectInputStream(fileIn);
            myAlbums = (ArrayList<Album>) in.readObject();
            in.close();
            fileIn.close();
        }catch (IOException e) {
            System.out.println("Error Reading File.");
        }catch (ClassNotFoundException e) {
            System.out.println("Class Not Found.");
        }

    }

    public void search(View view){
        String key = ((EditText)findViewById(R.id.key)).getText().toString();
        String value = ((EditText)findViewById(R.id.value)).getText().toString();

        if(key!=null&&value!=null) {
            if (key.toLowerCase().equals("location") || key.toLowerCase().equals("person")) {
                for(Album u:myAlbums){
                    for(Photo p:u.getPhotos()){
                        for(Tag t:p.getTags()){
                            System.out.println("TAG"+t.toString()+"PHOTO"+p.toString()+"ALBUM"+u.getName());
                            if(t.getType().toLowerCase().equals(key.toLowerCase())&&t.getValue().toLowerCase().startsWith(value.toLowerCase())){
                                found.add(p);
                                break;
                            }
                        }
                    }
                }
            }
            System.out.println(found.size());
        }

        Bundle b = new Bundle();
        b.putSerializable("results",found);
        Intent intent = new Intent(SearchActivity.this, ResultsActivity.class);
        intent.putExtras(b);
        startActivity(intent);




    }
}
