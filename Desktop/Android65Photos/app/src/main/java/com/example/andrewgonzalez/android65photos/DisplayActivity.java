package com.example.andrewgonzalez.android65photos;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Button;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

public class DisplayActivity extends AppCompatActivity {
    Album currAlbum;
    ArrayList<Album> albums;
    int index;
    ImageView display;
    Button prevButton;
    Button nextButton;
    TextView caption;
    ListView tags;

    String selectedTag;

    final Context context = this;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display);
        Intent intent = getIntent();
        Toolbar myToolbar = (Toolbar)findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);
        Bundle b = intent.getExtras();
        currAlbum = (Album)b.getSerializable("album");
        index = b.getInt("index");

        display = (ImageView) findViewById(R.id.imageView);
        prevButton = (Button) findViewById(R.id.button1);
        nextButton = (Button) findViewById(R.id.button2);
        caption = (TextView) findViewById(R.id.captionTextView);
        tags = (ListView) findViewById(R.id.tagsListView);
        tags.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,long arg3) {
                selectedTag = tags.getItemAtPosition(position).toString();
            }
        });

        File afile = new File(context.getFilesDir() + File.separator + "albums.dat");
        try {
            FileInputStream fileIn = new FileInputStream(afile);
            ObjectInputStream in = new ObjectInputStream(fileIn);
            albums = (ArrayList<Album>) in.readObject();
            in.close();
            fileIn.close();
        }catch (IOException e) {
            System.out.println("Error Reading File.");
        }catch (ClassNotFoundException e) {
            System.out.println("Class Not Found.");
        }
        updateDisplay();
    }

    private void updateDisplay() {
        Photo photo = currAlbum.getPhotos().get(index);
        File file = photo.getFile();
        display.setImageBitmap(BitmapFactory.decodeFile(file.getAbsolutePath()));
        caption.setText(photo.getCaption());

        ArrayList<String> tagStrings = new ArrayList<String>();
        for (Tag tag : photo.getTags()) {
            tagStrings.add(tag.toString());
        }
        ArrayAdapter<String> tagsAdapter = new ArrayAdapter<String> (this, R.layout.horizontal, tagStrings);
        tags.setAdapter(tagsAdapter);
    }

    public void addPerson(View view) {
        LayoutInflater li = LayoutInflater.from(context);
        View promptsView = li.inflate(R.layout.addperson, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setView(promptsView);
        final EditText userInput = (EditText) promptsView.findViewById(R.id.userInput);

        // set dialog message
        alertDialogBuilder.setCancelable(false).setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int id) {
                Tag tag = new Tag("person", userInput.getText().toString());
                currAlbum.getPhotos().get(index).addTag(tag);
                currAlbum.save(context);
                updateDisplay();
            }
        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int id) {
                dialog.cancel();
            }
        });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();
        updateDisplay();
        // show it
        alertDialog.show();
    }
    public void addLocation(View view) {
        LayoutInflater li = LayoutInflater.from(context);
        View promptsView = li.inflate(R.layout.addlocation, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setView(promptsView);
        final EditText userInput = (EditText) promptsView.findViewById(R.id.userInput);

        // set dialog message
        alertDialogBuilder.setCancelable(false).setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int id) {
                Tag tag = new Tag("location", userInput.getText().toString());
                currAlbum.getPhotos().get(index).addTag(tag);
                currAlbum.save(context);
                updateDisplay();
            }
        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int id) {
                dialog.cancel();
            }
        });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();
        updateDisplay();
        // show it
        alertDialog.show();
    }

    public void movePhoto (View view) {
        LayoutInflater li = LayoutInflater.from(context);
        View promptsView = li.inflate(R.layout.movephoto, null);
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setView(promptsView);

        final ListView albumListView = (ListView) promptsView.findViewById(R.id.albumListView);
        albumListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,long arg3) {
                String selectedAlbum = albumListView.getItemAtPosition(position).toString();
                for (Album album : albums) {
                    if (album.getName().equals(selectedAlbum)) {
                        album.addPhoto(currAlbum.getPhotos().get(index));
                        album.save(context);
                        currAlbum.deletePhoto(currAlbum.getPhotos().get(index));
                        currAlbum.save(context);

                        Bundle b = new Bundle();
                        b.putSerializable("album", currAlbum);
                        b.putInt("index", index);
                        Intent intent = new Intent(DisplayActivity.this, PhotoActivity.class);
                        intent.putExtras(b);
                        startActivity(intent);
                    }
                }
            }
        });

        ArrayList<String> albumStrings = new ArrayList<String>();
        for (Album album : albums) {
            albumStrings.add(album.getName());
        }
        ArrayAdapter<String> albumsAdapter = new ArrayAdapter<String> (this, R.layout.horizontal, albumStrings);
        albumListView.setAdapter(albumsAdapter);

        // set dialog message
        alertDialogBuilder.setCancelable(false).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int id) {
                dialog.cancel();
            }
        });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();
        updateDisplay();
        // show it
        alertDialog.show();
    }

    public void deleteTag (View view) {
        if (selectedTag == null) {
            Toast.makeText(DisplayActivity.this, "No tags to delete. click Add Person or Add Location to add a tag.", Toast.LENGTH_SHORT).show();
            return;
        }

        for (Tag tag : currAlbum.getPhotos().get(index).getTags()) {
            if (tag.toString().equals(selectedTag)) {
                currAlbum.getPhotos().get(index).deleteTag(tag);
                currAlbum.save(context);
                selectedTag = "";
                break;
            }
        }
        updateDisplay();
    }

    public void prevPhoto(View view) {
        if (index > 0) {
            index--;
        }
        updateDisplay();
    }

    public void nextPhoto(View view) {
        if (index < currAlbum.getNumPhoto()-1) {
            index++;
        }
        updateDisplay();
    }
}
