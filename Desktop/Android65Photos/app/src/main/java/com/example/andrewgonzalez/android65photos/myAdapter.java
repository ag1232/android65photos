package com.example.andrewgonzalez.android65photos;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;


public class myAdapter extends ArrayAdapter<Album> {
    public myAdapter(@NonNull Context context, @NonNull ArrayList<Album> objects) {

        super(context, R.layout.row_layout, objects);
    }


    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        LayoutInflater myInflater = LayoutInflater.from(getContext());
        View myview = myInflater.inflate(R.layout.row_layout,parent,false);
        String albumInfo = "\""+getItem(position).toString()+"\"";

        TextView myTextView = (TextView)myview.findViewById(R.id.textView1);

        myTextView.setText(albumInfo);

        return myview;
    }
}
