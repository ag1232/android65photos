package com.example.andrewgonzalez.android65photos;

import java.io.*;
/**
 * Class used for implementation of Tags
 * @author andrewgonzalez
 * @author danielrodriguez
 *
 */
public class Tag implements Serializable{
    /**
     * type corresponds to the type of tag such as 'location', 'person', etc.
     * value corresponds to the string value of the the tag such as the actual name/location.
     */
    String type;
    String value;
    private static final long serialVersionUID = 1L;

    public Tag (String type, String value){
        this.type = type;
        this.value = value;
    }
    public Tag (){
    }

    //used to edit the value of the tag
    public void setValue(String value){
        this.value = value;
    }

    //used to edit the type of tag
    public void setType(String type){
        this.type = type;
    }

    //returns type of tag
    public String getType(){
        return this.type;
    }

    //returns the value of then tag
    public String getValue(){
        return this.value;

    }

    /**
     * Used to override the .equals method to compare tags based on either:
     * 1) Compares both the tag type and tag value OR
     * 2) Compares tag type if comparing tag value isn't possible
     * 3) Compares Tag value if tag type isn't specified
     */
    public boolean equals(Tag tag){

        if( tag == null || !(tag instanceof Tag)){
            return true;
        }

        if (this.value.toLowerCase().trim().equals(tag.getValue().toLowerCase().trim()) && this.type.toLowerCase().trim().equals(tag.getType().toLowerCase().trim())) {
            return true;
        }

        else if (tag.getValue().equals("")) {
            if (this.type.toLowerCase().trim().equals(tag.getType().toLowerCase().trim())) {
                return true;
            }
        }

        else if (tag.getType().equals("")) {
            if (this.value.toLowerCase().trim().equals(tag.getValue().toLowerCase().trim())) {
                return true;
            }
        }

        return false;
    }

    public String toString () {
        return this.type + " : " + this.value;
    }
}
