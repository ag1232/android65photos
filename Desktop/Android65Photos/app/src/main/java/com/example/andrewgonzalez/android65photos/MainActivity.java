package com.example.andrewgonzalez.android65photos;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.content.DialogInterface.OnClickListener;
import android.widget.Toast;
import android.support.v7.widget.Toolbar;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.util.ArrayList;
import java.io.File;
import java.nio.file.Files;
import java.util.ArrayList;

//AppCompatActivity
public class MainActivity extends AppCompatActivity {

    final Context context = this;
    ArrayList<Album> myAlbums;
    ListAdapter AlbumAdapter;
    ListView AlbumListView;
    File afile;
    boolean er = false;
    //File copy = new File("PhotosCopy");


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar myToolbar = (Toolbar)findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        //populatePictureGallery();
       // File path = this.context.getFilesDir();
        //File path2 = getFilesDir();
        afile = new File(this.context.getFilesDir() + File.separator + "albums.dat");
        if(afile.exists()!=true){
            try {
                afile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        System.out.println(afile.exists());
        System.out.println("GAH");

        try {
            FileInputStream fileIn = new FileInputStream(afile);
            ObjectInputStream in = new ObjectInputStream(fileIn);
            myAlbums = (ArrayList<Album>) in.readObject();
            in.close();
            fileIn.close();
        }catch (IOException e) {
            System.out.println("Error Reading File.");
        }catch (ClassNotFoundException e) {
            System.out.println("Class Not Found.");
        }

        if(myAlbums == null){
            myAlbums = new ArrayList<Album>();
            Album a = new Album("test");
            myAlbums.add(a);
        }


        AlbumAdapter = new myAdapter(this, myAlbums);
        AlbumListView = (ListView) findViewById(R.id.AlbumListView);
        AlbumListView.setAdapter(AlbumAdapter);


        AlbumListView.setOnItemLongClickListener(new OnItemLongClickListener() {
            // setting onItemLongClickListener and passing the position to the function
            @Override
            public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
                                           int position, long arg3) {
                delete(position);

                return true;
            }
        });

        AlbumListView.setOnItemClickListener(new
                                                 AdapterView.OnItemClickListener() {
                                                     @Override
                                                     public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                                                         Bundle b = new Bundle();
                                                         b.putSerializable("album",(Album)AlbumListView.getItemAtPosition(i));
                                                         Intent intent = new Intent(MainActivity.this, PhotoActivity.class);
                                                         intent.putExtras(b);
                                                         startActivity(intent);
                                                     }
                                                 });

        if(ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
            != PackageManager.PERMISSION_GRANTED){
            System.out.println("Permission Denied");
            ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                1);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request.
        }
    }


    @Override
    protected void onStart() {
        super.onStart();
        try {
            FileInputStream fileIn = new FileInputStream(afile);
            ObjectInputStream in = new ObjectInputStream(fileIn);
            myAlbums = (ArrayList<Album>) in.readObject();
            in.close();
            fileIn.close();
        }catch (IOException e) {
            System.out.println("Error Reading File.");
        }catch (ClassNotFoundException e) {
            System.out.println("Class Not Found.");
        }

        AlbumAdapter = new myAdapter(this, myAlbums);
        AlbumListView = (ListView) findViewById(R.id.AlbumListView);
        AlbumListView.setAdapter(AlbumAdapter);

    }


    // method to remove list item
    protected void delete(int position) {
        final int deletePosition = position;

        AlertDialog.Builder alert = new AlertDialog.Builder(
            MainActivity.this);

        alert.setTitle("Delete");
        alert.setMessage("Do you want delete this item?");
        alert.setPositiveButton("YES", new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                try {
                    //FileInputStream fileIn = new FileInputStream("albums.dat");
                    //ObjectInputStream in = new ObjectInputStream(fileIn);
                    //myAlbums = (ArrayList<Album>) in.readObject();
                    //in.close();
                    //fileIn.close();
                    myAlbums.remove(deletePosition);
                    AlbumAdapter = new myAdapter(MainActivity.this, myAlbums);
                    AlbumListView.setAdapter(AlbumAdapter);
                    FileOutputStream fileOut = new FileOutputStream(afile);
                    ObjectOutputStream out = new ObjectOutputStream(fileOut);
                    out.writeObject(myAlbums);
                    out.close();
                    fileOut.close();


                }catch (IOException e) {
                    System.out.println("Error Writing File.");
                    // }catch (ClassNotFoundException e) {
                    //   System.out.println("Class Not Found.");
                }



            }
        });
        alert.setNegativeButton("CANCEL", new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        alert.show();
    }


    public void createB(View view){
        er = false;
        LayoutInflater li = LayoutInflater.from(context);
        View promptsView = li.inflate(R.layout.addalbum, null);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
            context);

        // set prompts.xml to alertdialog builder
        alertDialogBuilder.setView(promptsView);

        final EditText userInput = (EditText) promptsView
            .findViewById(R.id.albumInput);

        // set dialog message
        alertDialogBuilder
            .setCancelable(false)
            .setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {

                        Album newA = new Album(userInput.getText().toString());

                        for(Album u:myAlbums){
                            if(u.getName().equals(newA.getName())){
                                Toast.makeText(MainActivity.this,"ERROR: Album Exists",Toast.LENGTH_LONG);
                                er=true;
                                dialog.cancel();
                            }
                        }
                        if(er==false) {
                            try {
                                System.out.println("add");
                                // FileInputStream fileIn = new FileInputStream("albums.dat");
                                //ObjectInputStream in = new ObjectInputStream(fileIn);
                                //myAlbums = (ArrayList<Album>) in.readObject();
                                //in.close();
                                //fileIn.close();
                                myAlbums.add(newA);
                                AlbumAdapter = new myAdapter(MainActivity.this, myAlbums);
                                AlbumListView.setAdapter(AlbumAdapter);
                                FileOutputStream fileOut = new FileOutputStream(afile);
                                ObjectOutputStream out = new ObjectOutputStream(fileOut);
                                out.writeObject(myAlbums);
                                out.close();
                                fileOut.close();


                            } catch (IOException e) {
                                System.out.println("Error Reading File.");
                                //}catch (ClassNotFoundException e) {
                                //   System.out.println("Class Not Found.");
                            }
                        }






                    }
                })
            .setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        dialog.cancel();
                    }
                });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();

    }
    public void onSearch(View view){
        Intent intent = new Intent(MainActivity.this, SearchActivity.class);
        startActivity(intent);
    }

//    public void populatePictureGallery(){
//
//        ArrayList<File> fileList = new ArrayList<File>();
//       // final String dir = System.getProperty("user.dir");
//        //String test = path;
//        //System.out.println(path);
//
//
//        //File folder = new File(dir + "/src/main/java/Photos");
//        //listFilesForFolder(folder, fileList);
//        File root = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM + "/Camera/");
//        String path = root.getPath();
//        Uri data = Uri.parse(path);
//        File image = new File(data.getPath() + "/bk.png");
//        File root2 = new File("/data/media/");
//        File root3 = new File("/mnt/sdcard/test123/bb.png");
//        File root4 = new File("/mnt/user/");
//        File root5 = new File("/system/");
//        File root6 = Environment.getRootDirectory();
//        File directory1 = root.getAbsoluteFile();
//        File directory2 = new File(root2.getAbsolutePath()+"/Camera");
//        File directory3 = new File(root3.getAbsolutePath()+"/Camera");
//
//
//        String[] f1 = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM).list();
//        //System.out.println(copy.getAbsolutePath());
//
//
//        //File bfile = null;
//        //bfile = new File(this.context.getFilesDir() + File.separator + "albums.dat");
//        String directory = this.context.getFilesDir() + "/";
//        System.out.println(directory);
//
//        //final String dir2 = System.getProperty("user.dir");
//        //String directory = dir2 + "/src/zzzzzzz/copied";
//        //writeFiles(directory,fileList);
//
//    }
//
//    public void listFilesForFolder(File folder, ArrayList<File> fileList){
//       // R.
//        for (final File fileEntry : folder.listFiles()) {
//            System.out.println(fileEntry.getName());
//            File temp = fileEntry;
//            fileList.add(temp);
//
//        }
//    }
//
//    public void writeFiles(String directory, ArrayList<File> fileList){
//        for(int i= 0; i < fileList.size(); i++) {
//            String x = fileList.get(i).getName();
//            fileList.get(i).renameTo(new File(directory + "/" + x));
//        }
//    }

}

