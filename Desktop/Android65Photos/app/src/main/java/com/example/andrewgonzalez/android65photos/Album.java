package com.example.andrewgonzalez.android65photos;

import android.content.Context;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
/**
 * Used for album object implementation
 * @author andrewgonzalez
 * @author danielrodriguez
 */

public class Album implements Serializable{
    String name;
    Date oldest = null;
    Date latest = null;
    ArrayList<Photo> photos;
    private static final long serialVersionUID = 1L;


    public Album(String name, ArrayList<Photo> photos){
        this.name = name;
        this.photos = photos;
    }

    public Album(String name){
        this.name = name;
        photos = new ArrayList<Photo>();
    }

    /**adds Photo to the photos list aka the album
     */
    public void addPhoto(Photo p){
        photos.add(p);

        if (latest == null || p.getDate().compareTo(latest) > 0)
            latest=p.getDate();

        if (oldest == null || p.getDate().compareTo(oldest) < 0)
            oldest=p.getDate();
    }

    /**Removes Photo to the photos list aka the album
     */
    public void deletePhoto(Photo p){
        if(photos.contains(p)) {
            photos.remove(p);
            updateDates();
        }
    }

    public void updateDates(){
        if(photos.size()==0) {
            this.latest = null;
            this.oldest =null;
        }else {
            for(Photo p:photos) {
                if (oldest == null || p.getDate().compareTo(oldest) < 0)
                    oldest=p.getDate();

                if (latest == null || p.getDate().compareTo(latest) > 0)
                    latest=p.getDate();
            }
        }
    }


    /** Sets/edits the Album's name
     */
    public void setName(String name){
        this.name = name;
    }

    /** Returns the album name
     */
    public String getName(){
        return this.name;
    }


    /** Retuns the number of photos in the album
     */
    public int getNumPhoto(){
        return photos.size();
    }


    /** Returns the list of photos in the album
     */
    public ArrayList<Photo> getPhotos(){
        return this.photos;
    }


    /** Returns the latest date
     */
    public String getNewestDateString(){
        if (this.latest == null){
            return "";
        }
        return new SimpleDateFormat("MM/dd/yy").format(this.latest);
    }

    /** Returns the oldest date
     */
    public String getOldestDateString(){
        if (this.oldest == null){
            return "";
        }
        return new SimpleDateFormat("MM/dd/yy").format(this.oldest);
    }

    public String toString(){
        return this.name;
    }
    public void save (Context context) {
        ArrayList<Album> albums;
        File afile = new File(context.getFilesDir() + File.separator + "albums.dat");
        try {
            FileInputStream fileIn = new FileInputStream(afile);
            ObjectInputStream in = new ObjectInputStream(fileIn);
            albums = (ArrayList<Album>) in.readObject();
            in.close();
            fileIn.close();

            for (Album album : albums) {
                if (this.getName().equals(album.getName())){
                    albums.remove(album);
                    albums.add(this);
                    break;
                }
            }

            FileOutputStream fileOut = new FileOutputStream(afile);
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(albums);
            out.close();
            fileOut.close();
        }catch (IOException e) {
            System.out.println("Error Reading File.");
        }catch (ClassNotFoundException e) {
            System.out.println("Class Not Found.");
        }
    }
}
