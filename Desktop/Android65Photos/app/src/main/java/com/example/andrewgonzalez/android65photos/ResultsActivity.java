package com.example.andrewgonzalez.android65photos;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.ListAdapter;
import android.widget.ListView;

import java.util.ArrayList;

public class ResultsActivity extends AppCompatActivity {

    ListAdapter PhotoAdapter;
    ListView PhotoListView;
    ArrayList<Photo> results;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_results);
        Toolbar myToolbar = (Toolbar)findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        Bundle b = intent.getExtras();

        results = (ArrayList<Photo>) b.getSerializable("results");
        if(results!=null) {
            PhotoAdapter = new myPhotoAdapter(this, results);
            PhotoListView = (ListView) findViewById(R.id.results);
            PhotoListView.setAdapter(PhotoAdapter);
        }
    }
}

