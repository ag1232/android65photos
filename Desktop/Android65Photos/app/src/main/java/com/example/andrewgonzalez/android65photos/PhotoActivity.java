package com.example.andrewgonzalez.android65photos;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

public class PhotoActivity extends AppCompatActivity {

    final Context context = this;
    ArrayList<Album> myAlbums = new ArrayList<Album>();
    ArrayList<Photo> photos;
    Album currAlbum;
    ListAdapter PhotoAdapter;
    ListView PhotoListView;
    File afile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo);
        Toolbar myToolbar = (Toolbar)findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);
        Intent intent = getIntent();
        Bundle b = intent.getExtras();

        currAlbum = (Album) b.getSerializable("album");
        afile = new File("/data/user/0/com.example.andrewgonzalez.android65photos/files/albums.dat");
        //afile = new File("/data/user/0/android65photos/files/albums.dat");
        if(afile.exists()!=true){
            try {
                afile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        System.out.println(afile.exists());
        System.out.println("GAH");

        try {
            FileInputStream fileIn = new FileInputStream(afile);
            ObjectInputStream in = new ObjectInputStream(fileIn);
            myAlbums = (ArrayList<Album>) in.readObject();
            in.close();
            fileIn.close();
        }catch (IOException e) {
            System.out.println("Error Reading File.");
        }catch (ClassNotFoundException e) {
            System.out.println("Class Not Found.");
        }


        //if(currAlbum.getName()=="Default"&&currAlbum.getPhotos().isEmpty()) {
           /* File bfile = new File(context.getFilesDir() + File.separator + "20130818174759952.jpg");
            Photo p = new Photo(bfile);
            currAlbum.addPhoto(p);

            bfile = new File(context.getFilesDir() + File.separator + "1909254_681833838547716_1062608112_o.jpg");
            p = new Photo(bfile);
            currAlbum.addPhoto(p);

            bfile = new File(context.getFilesDir() + File.separator + "1920323_662063653840256_278456884_n.png");
            p = new Photo(bfile);
            currAlbum.addPhoto(p);

            bfile = new File(context.getFilesDir() + File.separator + "1957993_661952553851366_1806715131_n.jpg");
            p = new Photo(bfile);
            currAlbum.addPhoto(p);

            bfile = new File(context.getFilesDir() + File.separator + "68799999.JPG");
            p = new Photo(bfile);
            currAlbum.addPhoto(p);

        try {
            System.out.println("add");
            for (Album u : myAlbums) {
                if (currAlbum.getName().equals(u.getName())) {

                    myAlbums.set(myAlbums.indexOf(u), currAlbum);
                }
            }
            FileOutputStream fileOut = new FileOutputStream(afile);
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(myAlbums);
            out.close();
            fileOut.close();


        }catch (IOException e) {
            System.out.println("Error Reading File.");
            //}catch (ClassNotFoundException e) {
            //   System.out.println("Class Not Found.");
        }

        //}*/




        PhotoAdapter = new myPhotoAdapter(this, currAlbum.getPhotos());
        PhotoListView = (ListView) findViewById(R.id.PList);
        PhotoListView.setAdapter(PhotoAdapter);


        PhotoListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            // setting onItemLongClickListener and passing the position to the function
            @Override
            public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
                                           int position, long arg3) {
                delete(position);

                return true;
            }
        });

        PhotoListView.setOnItemClickListener(new
                                                 AdapterView.OnItemClickListener() {
                                                     @Override
                                                     public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                                                         Bundle b = new Bundle();
                                                         b.putSerializable("album",currAlbum);
                                                         b.putInt("index",i);
                                                         Intent intent = new Intent(PhotoActivity.this, DisplayActivity.class);
                                                         intent.putExtras(b);
                                                         startActivity(intent);
                                                     }
                                                 });
    }


    // method to remove list item
    protected void delete(int position) {
        final int deletePosition = position;

        AlertDialog.Builder alert = new AlertDialog.Builder(
            PhotoActivity.this);

        alert.setTitle("Delete");
        alert.setMessage("Do you want delete this item?");
        alert.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                try {
                    //FileInputStream fileIn = new FileInputStream("albums.dat");
                    //ObjectInputStream in = new ObjectInputStream(fileIn);
                    //myAlbums = (ArrayList<Album>) in.readObject();
                    //in.close();
                    //fileIn.close();
                    currAlbum.getPhotos().remove(deletePosition);

                    for (Album u : myAlbums) {

                        if (currAlbum.getName().equals(u.getName())) {
                            myAlbums.set(myAlbums.indexOf(u), currAlbum);
                        }
                    }

                    FileOutputStream fileOut = new FileOutputStream(afile);
                    ObjectOutputStream out = new ObjectOutputStream(fileOut);
                    out.writeObject(myAlbums);
                    out.close();
                    fileOut.close();

                    PhotoAdapter = new myPhotoAdapter(PhotoActivity.this, currAlbum.getPhotos());
                    PhotoListView.setAdapter(PhotoAdapter);

                }catch (IOException e) {
                    System.out.println("Error Writing File.");
                    // }catch (ClassNotFoundException e) {
                    //   System.out.println("Class Not Found.");
                }



            }
        });
        alert.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        alert.show();
    }


    public void createP(View view){
        LayoutInflater li = LayoutInflater.from(context);
        View promptsView = li.inflate(R.layout.addalbum, null);


        Bundle b = new Bundle();
        b.putSerializable("album",currAlbum);
        Intent intent = new Intent(PhotoActivity.this, GalleryActivity.class);
        intent.putExtras(b);
        startActivity(intent);
        /*AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                context);

        // set prompts.xml to alertdialog builder
        alertDialogBuilder.setView(promptsView);

        final EditText userInput = (EditText) promptsView
                .findViewById(R.id.albumInput);

        // set dialog message
        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                File f = new File(userInput.getText().toString());
                                Photo newp = new Photo(f);
                                try {
                                    System.out.println("add");
                                    // FileInputStream fileIn = new FileInputStream("albums.dat");
                                    //ObjectInputStream in = new ObjectInputStream(fileIn);
                                    //myAlbums = (ArrayList<Album>) in.readObject();
                                    //in.close();
                                    //fileIn.close();
                                    currAlbum.addPhoto(newp);
                                    PhotoAdapter = new myAdapter(PhotoActivity.this, myAlbums);
                                    PhotoListView.setAdapter(PhotoAdapter);
                                    for (Album u : myAlbums) {
                                        if (currAlbum.equals(u)) {
                                            myAlbums.set(myAlbums.indexOf(u), currAlbum);
                                        }
                                    }
                                    FileOutputStream fileOut = new FileOutputStream(afile);
                                    ObjectOutputStream out = new ObjectOutputStream(fileOut);
                                    out.writeObject(myAlbums);
                                    out.close();
                                    fileOut.close();


                                }catch (IOException e) {
                                    System.out.println("Error Reading File.");
                                    //}catch (ClassNotFoundException e) {
                                    //   System.out.println("Class Not Found.");
                                }







                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                dialog.cancel();
                            }
                        });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    */
    }

    public void edit(View view){
        LayoutInflater li = LayoutInflater.from(context);
        View promptsView = li.inflate(R.layout.addalbum, null);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
            context);

        // set prompts.xml to alertdialog builder
        alertDialogBuilder.setView(promptsView);
        alertDialogBuilder.setTitle("Edit: "+currAlbum.getName());

        final EditText userInput = (EditText) promptsView
            .findViewById(R.id.albumInput);

        // set dialog message
        alertDialogBuilder
            .setCancelable(false)
            .setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        try {
                            System.out.println("edit");
                            // FileInputStream fileIn = new FileInputStream("albums.dat");
                            //ObjectInputStream in = new ObjectInputStream(fileIn);
                            //myAlbums = (ArrayList<Album>) in.readObject();
                            //in.close();
                            //fileIn.close();


                            try{
                                for (Album u : myAlbums) {
                                    if (currAlbum.getName().equals(u.getName())) {
                                        myAlbums.remove(u);
                                        //System.out.println(currAlbum.getName()+"   "+u.getName());
                                        break;
                                    }
                                }

                            }catch (NullPointerException e){
                                ArrayList<Album> cool = myAlbums;
                            }
//                            for (Album u : myAlbums) {
//                                if (currAlbum.getName().equals(u.getName())) {
//                                    myAlbums.remove(u);
//                                    //System.out.println(currAlbum.getName()+"   "+u.getName());
//                                    break;
//                                }
//                            }
                            currAlbum.setName(userInput.getText().toString());
                            myAlbums.add(currAlbum);

//                            if(afile.exists()!=true){
//                                try {
//                                    afile.createNewFile();
//                                } catch (IOException e) {
//                                    e.printStackTrace();
//                                }
//                            }

                            FileOutputStream fileOut = new FileOutputStream(afile);
                            ObjectOutputStream out = new ObjectOutputStream(fileOut);
                            out.writeObject(myAlbums);
                            out.close();
                            fileOut.close();

                            Intent intent = new Intent( PhotoActivity.this, MainActivity.class);
                            startActivity(intent);
                            //PhotoAdapter = new myAdapter(PhotoActivity.this, myAlbums);
                            //PhotoListView.setAdapter(PhotoAdapter);

                        }catch (IOException e) {
                            System.out.println("Error Reading File.");
                            //}catch (ClassNotFoundException e) {
                            //   System.out.println("Class Not Found.");
                        }







                    }
                })
            .setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        dialog.cancel();
                    }
                });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();



    }
}
