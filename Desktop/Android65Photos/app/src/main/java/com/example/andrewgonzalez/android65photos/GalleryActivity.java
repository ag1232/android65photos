package com.example.andrewgonzalez.android65photos;

import android.content.Context;
import android.content.Intent;
import android.os.Environment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class GalleryActivity extends AppCompatActivity {

    final Context context = this;
    ArrayList<Album> myAlbums;
    ArrayList<Photo> photos;
    ListAdapter PhotoAdapter;
    ListView PhotoListView;
    File libDir;
    Album currAlbum;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery);
        Toolbar myToolbar = (Toolbar)findViewById(R.id.my_toolbar);
       setSupportActionBar(myToolbar);
        ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);
        Intent intent = getIntent();
        Bundle b = intent.getExtras();
        currAlbum = (Album) b.getSerializable("album");
        photos = new ArrayList<Photo>();


        // File rootsd = Environment.getExternalStorageDirectory();
        // File root = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM + "/Camera/");
        libDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM + "/Camera/");

        // libDir = new File("/data/data/android65photos/files/");

        if (libDir.exists() != true) {
            try {
                System.out.println(libDir.toString());
                libDir.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if(libDir.isDirectory()) {
            System.out.println(libDir.toString());
            //Add an if or try catch here for the case that there are currently no photos on the phone and we have to catch file [] being null
            String[] files = libDir.list();
            //This if-statement test whether or not there are any photos on the current phone, if there are then go through the list of pictures
            if (files != null){
                System.out.println(files.toString());
                for (int i = 0; i < files.length; ++i) {
                    //File temp = new File("/data/data/android65photos/files/"+files[i]);
                    File temp = new File(libDir.getAbsolutePath() + "/" + files[i]);
                    System.out.println(temp.toString());
                    if (!temp.isDirectory()&&accept(temp)) {
                        Photo p = new Photo(temp);
                        photos.add(p);
                    }
                }
            }
            //IF there are no current pictures in the phone gallery notify user.
            else{
                Toast.makeText(GalleryActivity.this, "You currently don't have any photos to choose from. Take pictures with the camera app!", Toast.LENGTH_SHORT).show();

            }


        }

        PhotoAdapter = new myPhotoAdapter(this, photos);
        PhotoListView = (ListView) findViewById(R.id.P2);
        PhotoListView.setAdapter(PhotoAdapter);



        PhotoListView.setOnItemClickListener(new
                                                 AdapterView.OnItemClickListener() {
                                                     @Override
                                                     public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                                                         currAlbum.addPhoto((Photo) PhotoListView.getItemAtPosition(i));
                                                         currAlbum.save(context);
                                                         Bundle b = new Bundle();
                                                         b.putSerializable("album",currAlbum);
                                                         Intent intent = new Intent(GalleryActivity.this, PhotoActivity.class);
                                                         intent.putExtras(b);
                                                         startActivity(intent);
                                                     }
                                                 });
    }



    /**
     *
     */

    public boolean accept(File file)
    {
        String[] okFileExtensions =  new String[] {"jpg", "png", "jpeg"};
        for (String extension : okFileExtensions)
        {
            if (file.getName().toLowerCase().endsWith(extension))
            {
                return true;
            }
        }
        return false;
    }


}

